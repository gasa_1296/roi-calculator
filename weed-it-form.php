<?php

/**
 * Plugin Name: WeedIt form
 * Plugin URI: https://wodeva.com/
 * Description: WeedIt form.
 * Author: Gabriel Segovia
 * Author URI: https://wodeva.com/
 * Version: 0.7
 */


add_action('wp_loaded', function() {
  wp_enqueue_script('WI_script', plugin_dir_url(__FILE__) . 'assets/script.js');
  wp_enqueue_style('WI_style', plugin_dir_url(__FILE__) . 'assets/style.css');
});
?>
