window.addEventListener('DOMContentLoaded', () => {
  const inputs = {
    hectareas: document.getElementById('WI_superficie'), 
    pasadas: document.getElementById('WI_pasadas'),
    nivel: document.getElementById('WI_nivel'),
    promedio: document.getElementById('WI_promedio'),
  }

  document.getElementById("CF_link").style.display = 'none';

  Object.keys(inputs).forEach(key => {
    inputs[key].addEventListener('change', () => {

      let totalPulverizar = inputs.hectareas.value * inputs.pasadas.value;

      if (totalPulverizar > 0) {
        document.getElementById('WI_total').value = totalPulverizar;
        let nivel = 0.6;
        if (inputs.nivel.value == 'Bajo') nivel = 0.85;
        if (inputs.nivel.value == 'Medio') nivel = 0.75;

        let ahorro = totalPulverizar * nivel * inputs.promedio.value;

        if (ahorro > 0) {
          document.getElementById('WI_ahorro').value = ahorro + 'USD';
          document.getElementById("CF_link").style.display = 'block';
        }
        else {
          document.getElementById("CF_link").style.display = 'none';
        }
      }

    });
  });

  document.getElementById('CF_show').addEventListener('click', () => {
    document.getElementById('CF_shordcode').style.display = 'block'
  });
});
